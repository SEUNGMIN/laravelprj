<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker::create();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Model::unguard();

        App\User::truncate();
        factory(App\User::class)->create([
            'name' => 'john doe',
            'email' => 'john@example.com',
            'password' => bcrypt('123qwe')
        ]);

        factory(App\User::class, 9)->create();
        $this->command->info('users table seeded');

        //Bican\Roles\Models\Role::truncate();
        jeremykenedy\LaravelRoles\Models\Role::truncate();
        DB::table('role_user')->truncate();
        $adminRole = jeremykenedy\LaravelRoles\Models\Role::created([
            'name' => 'Admin',
            'slug' => 'admin'
        ]);
        /*$adminRole = jeremykenedy\LaravelRoles\Models\Role::created([
            'ADMIN' => 'Admin',
        ]);*/

        $memberRole = jeremykenedy\LaravelRoles\Models\Role::created([
            'name' => 'Member',
            'slug' => 'member'
        ]);
        echo "adminRole " .print_r($adminRole)."<br>";
        echo "memberRole " .print_r($memberRole)."<br>";
        App\User::where('email', '!=', 'john@example.com')->get()->map(function($user) use($memberRole) {
            $user->attachRole($memberRole);
        });

        App\User::whereEmail('john@example.com')->get()->map(function($user) use($adminRole){
            $user->attachRole($adminRole);
        });

    }
}
