<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use jeremykenedy\LaravelRoles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Model implements AuthenticatableContract,
    CanResetPasswordContract,
    HasRoleAndPermissionContract
{
    use Authenticatable;
    use CanResetPassword;
    use HasRoleAndPermission;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'deptcode', 'password', 'admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function DEPTNAME()
    {
        return $this->belongsTo(DEPART::class,'dept_name');
    }

    public function DEPTCODE()
    {
        return $this->belongsTo(DEPART::class,'dept_code');
    }

    public function REFERENCES()
    {
        return $this->hasMany(ROOMA::class,'book_member');
    }

    public function DOWNLOAD()
    {
        return $this->hasMany(DOWNLOAD::class,'download_writer');
    }

    public function isAdmin()
    {
        return $this->roles()->whereSlug('admin')->exists();
    }
}
